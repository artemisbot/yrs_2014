<html>
    <head>
		<?php
			//echo '<p>Initialising...</p>';	
			require_once('./php-twilio/Twilio.php');
			//echo '<p> Twilio loaded....</p>';			
			$con=mysqli_connect("localhost","###","###","###");
			//echo '<p>MySql loaded...</p>';
			// Check connection
			if (mysqli_connect_errno()) {
			  echo "Failed to connect to MySQL: " . mysqli_connect_error();
			}

			// Create table
			$firstname = mysqli_real_escape_string($con, $_POST['FirstName']);
			$mobnum = mysqli_real_escape_string($con, $_POST['MobNum']);
			$startstation = mysqli_real_escape_string($con, $_POST['StartStation']);
			$endstation = mysqli_real_escape_string($con, $_POST['EndStation']);
			$morntime = mysqli_real_escape_string($con, $_POST['MornTime']);
			$nighttime = mysqli_real_escape_string($con, $_POST['NightTime']);
			$mobnum = "+44" . substr($mobnum, 1);
			$sql="INSERT INTO user (FirstName, MobNum, StartStation, EndStation, MornTime, AfterTime)
			VALUES ('$firstname', '$mobnum', '$startstation', '$endstation', '$morntime', '$nighttime')";
			if (!mysqli_query($con,$sql)) {
			  die('Error: ' . mysqli_error($con));
			}
			
			mysqli_close($con);
			// this line loads the library  
			$account_sid = '###';
			$auth_token = '###';
			$client = new Services_Twilio($account_sid, $auth_token); 
			 
			$client->account->messages->create(array( 
				'To' => "$mobnum", 
				'From' => "+441586202028", 
				'Body' => "Thanks for signing up to Commutifier $firstname! You have selected journey $startstation to $endstation at times $morntime and $nighttime!",   
			));
		?>
        <meta charset="UTF-8">
        <meta http-equiv="refresh" content="1;url=http://commutifier.artemisbot.co.uk/thanks.html">
        <script type="text/javascript">
            window.location.href = "http://commutifier.artemisbot.co.uk/thanks.html"
        </script>
        <title>Page Redirection</title>
    </head>
    <body>
        <!-- Note: don't tell people to `click` the link, just tell them that it is a link. -->
        If you are not redirected automatically, follow the <a href='http://commutifier.artemisbot.co.uk/thanks.html'>link to the thanks page.</a>
    </body>
</html>